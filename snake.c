#include <ncurses.h>
#include <stdlib.h>

/*
*
* War ne blöde Idee, wirklich. (Hätte nicht gedacht, dass da so viel dranhängt...)
*
*/

int getCleanPress(int* lp){
	int d = getch();
	
	if(d == KEY_LEFT || d == KEY_RIGHT || d == KEY_UP || d == KEY_DOWN || d == 'p'){
		if((*lp == KEY_LEFT && d == KEY_RIGHT) || (*lp == KEY_RIGHT && d == KEY_LEFT)
			|| (*lp == KEY_DOWN && d == KEY_UP) || (*lp == KEY_UP && d == KEY_DOWN)){
				return *lp;
			}else{
				*lp = d;
				return d;
			}
	}else{
		return *lp;
	}
}

void initScreen(){
	WINDOW  *w;
	
	w = initscr();
	cbreak();
	noecho();
	nodelay(w, TRUE);
	keypad(stdscr, TRUE);
	curs_set(0);
	
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_RED);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	init_pair(3, COLOR_YELLOW, COLOR_GREEN);
	init_pair(4, COLOR_RED, COLOR_BLUE);
	init_pair(5, COLOR_WHITE, COLOR_GREEN);
	init_pair(6, COLOR_BLUE, COLOR_BLACK);
	init_pair(7, COLOR_RED, COLOR_BLACK);
}

void showDifficultySelection(int* di, int maxr, int maxc){
	attron(COLOR_PAIR(5));
	mvprintw(maxr/2, maxc/2 - 9, "Welcome to cSnake!");
	attroff(COLOR_PAIR(5));
	attron(COLOR_PAIR(2));
	mvprintw(maxr/2 + 1, maxc/2 - 32, "Please enter your desired difficulty: (e)asy, (m)oderate or (f)ast\n");
	timeout(-1);
	int g = getch();
	
	switch(g){
		case 'e': *di = 13;
			break;
		case 'f': *di = 8;
			break;
			
		case 'm': 
		default: *di = 10;
	}
}

void displayDeathMessage(int maxr, int maxc, int* p, int fe, int t){
	attron(COLOR_PAIR(1));
	mvprintw(maxr/2, maxc/2 - 5, "YOU LOST!");
	attroff(COLOR_PAIR(1));
	attron(COLOR_PAIR(2));
	mvprintw(maxr/2 + 1, maxc/2 - 10, "Press y to play again");
	attroff(COLOR_PAIR(2));
	attron(COLOR_PAIR(6));
	mvprintw(maxr/2 + 3, maxc/2 - 7, "Your Score: ");
	mvprintw(maxr/2 + 3, maxc/2 + 5, "%d", (t * fe) / 2);
	attroff(COLOR_PAIR(6));
	
	refresh();
				
	*p = 0;
}

void initSnake(int** snake, int r, int c, int** csnake, int* e){
	snake[0][0] = r / 2;
	snake[0][1] = c / 2 - 1;
	snake[1][0] = r / 2;
	snake[1][1] = c / 2;
	snake[2][0] = r / 2;
	snake[2][1] = c / 2 + 1;
	
	csnake[0][0] = r / 2 + 3;
	csnake[0][1] = c / 2 - 1;
	csnake[1][0] = r / 2 + 3;
	csnake[1][1] = c / 2;
	csnake[2][0] = r / 2 + 3;
	csnake[2][1] = c / 2 + 1;
	
	*e = 1;
}

void respawnCSnake(int** csnake, int* csl, int r, int c){
	*csl = 3;
	
	csnake[0][0] = r / 2 + 3;
	csnake[0][1] = c / 2 - 1;
	csnake[1][0] = r / 2 + 3;
	csnake[1][1] = c / 2;
	csnake[2][0] = r / 2 + 3;
	csnake[2][1] = c / 2 + 1;
}

void placeWalls(int** walls, int maxr, int maxc){
	int i, c, x, y, **temp, j;
	c = 0;
	
	while(c > 20 || c < 6){
		c = rand() % 20;
	}
	
	temp = (int**) calloc(maxr + 10, sizeof(int*));
	for(i = 0; i < maxr + 10; i++){
		temp[i] = (int*) calloc(maxc + 10, sizeof(int));
	}
	
	//Were supposed to be 4x4 blocks once. Once.
	for(i = 0; i < c / 4; i++){
	A:	y = rand() % maxr;
			x = rand() % maxc;
		
		if(temp[y][x] != 205 && y > 2 && x > 2){
				temp[y][x] = 205;
				temp[y - 1][x] = 205;
				temp[y + 1][x + 1] = 205;
				temp[y - 1][x - 1] = 205;
		}else{
			goto A;
		}
	}
	
	//Wände horizontal
	for(i = 0; i < c / 2; i++){
	B:	y = rand() % maxr;
		x = rand() % maxc;
		
		if(temp[y][x] != 205 && y > 2 && x > 2){
			for(j = 0; j < 7; j++){
				temp[y][x + j] = 205;
			}
			
		}else{
			goto B;
		}
	}
	
	//Wände senkrecht
	for(i = 0; i < c / 2; i++){
	C:	y = rand() % maxr;
		x = rand() % maxc;
		
		if(temp[y][x] != 205 && y > 2 && x > 2){
			for(j = 0; j < 7; j++){
				temp[y + j][x] = 205;
			}
			
		}else{
			goto C;
		}
	}
	
	//Outer Walls
	for(i = 2; i < maxr - 2; i++){
		if(rand() % 101 > 5){
			temp[i][1] = 205;
			temp[i][maxc - 2] = 205;
		}
	}

	for(i = 1; i < maxc - 1; i++){
		if(rand() % 101 > 2){
			temp[2][i] = 205;
			temp[maxr - 2][i] = 205;
		}
	}
	
	//Copy temp to working value stripping excess blocks
	for(i = 0; i < maxr - 1; i++){
		for(j = 0; j < maxc - 1; j++){
			if(temp[i][j] != 0)
				walls[i][j] = temp[i][j];
		}
	}
	
	free(temp);
}

void spawnFood(int food[][2], int maxr, int maxc, int** walls, int* e, int* s, int* sf, int c){
J:	if(*e){
		int y = rand() % maxr;
		int x = rand() % maxc;
		
		if(x < 3)
			goto J;
			
		if(y < 2)
			goto J;
		
		if(walls[y][x] != 0)
			goto J;
		
		food[0][0] = y;
		food[0][1] = x;
		*e = 0;
	}
	
	if(*s && c == 0){
		if(random() % 101 < 2){
	X:	if(1){	
			int y = rand() % maxr;
			int x = rand() % maxc;
			
			if(x < 3)
				goto X;
			
			if(y < 2)
				goto X;
		
			if(walls[y][x] != 0)
				goto X;
			
			food[1][0] = y;
			food[1][1] = x;
			*s = 0;
			
			if(random() % 101 < 50){
				*sf = 1; 
			}else{
				*sf = 2;
			}	
		}
		}
	}
}

void checkEat(int** snake, int food[][2], int* sl, int* f, int d, int** csnake, int* cs, int* e, int* s, int* c){
	if(snake[0][0] == food[0][0] && snake[0][1] == food[0][1]){
		(*sl)++;
		(*f)++;
		*e = 1;
		
		snake[*sl - 1][0] = snake[*sl - 2][0];
		snake[*sl - 1][1] = snake[*sl - 2][1];
	}

	if(csnake[0][0] == food[0][0] && csnake[0][1] == food[0][1]){
 			(*cs)++;
 			*e = 1;
 			csnake[*cs - 1][0] = csnake[*cs - 2][0];
			csnake[*cs - 1][1] = csnake[*cs - 2][1];
	}
	
	if(snake[0][0] == food[1][0] && snake[0][1] == food[1][1]){
		(*sl)++;
		(*f)++;
		*s = 1;
		*c = 50;
		
		snake[*sl - 1][0] = snake[*sl - 2][0];
		snake[*sl - 1][1] = snake[*sl - 2][1];
	}
}

int timer(int cdi, int* t, int* x){
	if(*x >= cdi){
		*t = *t + 1;
		*x = 0;
	}else{
		*x = *x + 1;
	}
	return *t;
}

void checkStatus(int* sf, int* di, int* c, int maxc, int* odi){
	if(*c > 0){
		if(*sf == 1){
			attron(COLOR_PAIR(6));
			mvprintw(0, maxc/2 - 3, "Slower");
			attroff(COLOR_PAIR(6));
			*di = 20;
		}
		if(*sf == 2){
			attron(COLOR_PAIR(7));
			mvprintw(0, maxc/2 - 3, "Faster");
			attroff(COLOR_PAIR(7));
			*di = 3;
		}
		(*c)--;
	}else{
		*di = *odi;
	}
}

//RIP :(
void checkDed(int** snake, int** csnake, int** walls, int* sl, int* csl, int* fe, int maxr, int maxc, int* p, int t){
	int i, j;

	if(walls[snake[0][0]][snake[0][1]] == 205){
		displayDeathMessage(maxr, maxc, p, *fe, t);
	}
	
	if(walls[csnake[0][0]][csnake[0][1]] == 205){
		respawnCSnake(csnake, csl, maxr, maxc);
	}
	
	for(i = 0; i < *sl; i++){
		for(j = 0; j < *sl; j++){
			if(snake[i][0] == snake[j][0] && snake[i][1] == snake[j][1] && j != i){
				displayDeathMessage(maxr, maxc, p, *fe, t);
			}
		}
		
		if(csnake[0][0] == snake[i][0] && csnake[0][1] == snake[i][1]){
			respawnCSnake(csnake, csl, maxr, maxc);
			*fe += 4;
		}
		
	}
	
	for(i = 0; i < *csl; i ++){
		if(snake[0][0] == csnake[i][0] && snake[0][1] == csnake[i][1]){
			displayDeathMessage(maxr, maxc, p, *fe, t);
		}
	}

}

void drawWalls(int** walls, int fe, int maxr, int maxc, int p, int cdi, int* t, int* xt){
	int x, y;
	if(p){
		for(y = 0; y < maxr; y++){
			for(x = 0; x < maxc; x++){
				if(walls[y][x] != 0){
					move(y, x);
					addch(205);
				}
			}
		}
		
		attron(COLOR_PAIR(2));
		mvprintw(0, 1, "Points: %d", fe);
		attroff(COLOR_PAIR(2));
		attron(COLOR_PAIR(6));
		mvprintw(0, maxc - 12, "Time: %d", timer(cdi, t, xt));
		attroff(COLOR_PAIR(6));
		
		refresh();
	}
}

void draw(int** snake, int food[][2], int sl, int** csnake, int csl, int p, int s, int sf){
	int i;
	if(p){
		erase();
		move(food[0][0], food[0][1]);
		attron(COLOR_PAIR(2));
		addch('#');
		attroff(COLOR_PAIR(2));

		for(i = 0; i < sl; i++){
			move(snake[i][0], snake[i][1]);
			attron(COLOR_PAIR(3));
			addch('O');
		}
		attroff(COLOR_PAIR(3));

		for(i = 0; i < csl; i++){
			move(csnake[i][0], csnake[i][1]);
			attron(COLOR_PAIR(4));
			addch('X');
		}
		attroff(COLOR_PAIR(4));
		
		if(!s){
			move(food[1][0], food[1][1]);
			if(sf == 1){
				attron(COLOR_PAIR(6));
				addch('a');
				attroff(COLOR_PAIR(6));
			}else{
				attron(COLOR_PAIR(7));
				addch('s');
				attroff(COLOR_PAIR(7));
			}
		}
	
	doupdate();
	}
}

void moveCSnake(int** csnake, int csl, int maxr, int maxc, int food[][2], int** walls, int* lpc){
	int r, i, x;	
	r = KEY_DOWN;
	x = 0;
		
	//Finds the way to the food
	if(food[0][1] > csnake[0][1]){
			r = KEY_RIGHT;
	}else if(food[0][1] < csnake[0][1]){
			r = KEY_LEFT;
	}
	else{
		if(food[0][0] > csnake[0][0]){
			r = KEY_DOWN;
		}
		if(food[0][0] < csnake[0][0]){
			r = KEY_UP;
		}
	}
	
	//Avoids collisions (mostly.) Still has errors, and can cause a loop
	switch(r){
 		case KEY_LEFT:
 			if(walls[csnake[0][0]][csnake[0][1] - 1] != 0){
 				r = KEY_UP;
				x = 1;
				if(walls[csnake[0][0] - 1][csnake[0][1]] != 0){
					r = KEY_DOWN;
				}
 			}
 		break;
			
 		case KEY_RIGHT:
 			if(walls[csnake[0][0]][csnake[0][1] + 1] != 0){
 				r = KEY_UP;
				x = 1;
				if(walls[csnake[0][0] - 1][csnake[0][1]] != 0){
					r = KEY_DOWN;
				}
			}
 		break;
 			
 		case KEY_UP:
 			if(walls[csnake[0][0] - 1][csnake[0][1]] != 0){
 				r = KEY_RIGHT;
				x = 1;
				if(walls[csnake[0][0]][csnake[0][1] + 1] != 0){
					r = KEY_LEFT;
				}
 			}
 		break;
 			
 		case KEY_DOWN:
 			if(walls[csnake[0][0] + 1][csnake[0][1]] != 0){
 				r = KEY_LEFT;
				x = 1;
				if(walls[csnake[0][0]][csnake[0][1] - 1] != 0){
					r = KEY_RIGHT;
				}
 			}
 		break;
	}
	
	if(random() % 101 <= 10 && *lpc != r && x != 1){
		if(r == KEY_LEFT){
			r = KEY_UP;
		}else{
			r = KEY_LEFT;
		}
	}
		
	//So that AI cant "cheat"
	if(*lpc == KEY_DOWN && r == KEY_UP){
		r = KEY_LEFT;
	}
	if(*lpc == KEY_UP && r == KEY_DOWN){
		r = KEY_RIGHT;
	}
	if(*lpc == KEY_RIGHT && r == KEY_LEFT){
		r = KEY_DOWN;
	}
	if(*lpc == KEY_LEFT && r == KEY_RIGHT){
		r = KEY_UP;
	}
	
	*lpc = r;
	
	for(i = csl - 1; i > 0; i--){
 		csnake[i][0] = csnake[i - 1][0];
		csnake[i][1] = csnake[i - 1][1];
 	}

	switch(r){
		case KEY_LEFT:
			if(csnake[0][1] - 1 > 2){
				csnake[0][1] = csnake[0][1] - 1;
			}else{
				csnake[0][1] = maxc - 2;
			}
		break;
			
		case KEY_RIGHT:
			if(csnake[0][1] + 1 < maxc - 2){
				csnake[0][1] = csnake[0][1] + 1;
			}else{
				csnake[0][1] = 2;
			}
		break;
			
		case KEY_UP:
			if(csnake[0][0] - 1 > 1){
				csnake[0][0] = csnake[0][0] - 1;
			}else{
				csnake[0][0] = maxr - 1;
			}
		break;
			
		case KEY_DOWN:
			if(csnake[0][0] + 1 < maxr - 1){
				csnake[0][0] = csnake[0][0] + 1;
			}else{
				csnake[0][0] = 1;
			}
		break;
	}
}

void moveSnake(int** snake, int d, int sl, int maxr, int maxc){
	int i;

	for(i = sl - 1; i > 0; i--){
		snake[i][0] = snake[i - 1][0];
		snake[i][1] = snake[i - 1][1];
	}

	switch(d){
		case KEY_LEFT:
			if(snake[0][1] - 1 > 2){
				snake[0][1] = snake[0][1] - 1;
			}else{
				snake[0][1] = maxc - 2;
			}
		break;
			
		case KEY_RIGHT:
			if(snake[0][1] + 1 < maxc - 2){
				snake[0][1] = snake[0][1] + 1;
			}else{
				snake[0][1] = 2;
			}
		break;
			
		case KEY_UP:
			if(snake[0][0] - 1 > 1){
				snake[0][0] = snake[0][0] - 1;
			}else{
				snake[0][0] = maxr - 2;
			}
		break;
			
		case KEY_DOWN:
			if(snake[0][0] + 1 < maxr - 1){
				snake[0][0] = snake[0][0] + 1;
			}else{
				snake[0][0] = 1;
			}
		break;
	}
}

int main(void){
	int **snake, **csnake, **walls, food[2][2], sl, csl, fe, k, maxr, maxc, i, e, p, lpc, lp, di, s, sf, sc, cdi, t, xt;
	srand(time(NULL));

	initScreen();

	getmaxyx(stdscr, maxr, maxc);
	showDifficultySelection(&di, maxr, maxc);
	cdi = di;
	
	while(1){
		//Well, in this case it doesnt get longer. its the way it is.
		snake = (int**) calloc(40, sizeof(int*));
			for(i = 0; i < 40; i++){
				snake[i] = (int*) calloc(2, sizeof(int));
			}
			
		csnake = (int**) calloc(40, sizeof(int*));
			for(i = 0; i < 40; i++){
				csnake[i] = (int*) calloc(2, sizeof(int));
  			}
			
		walls = (int**) calloc(maxr, sizeof(int*));
			for(i = 0; i < maxr; i++){
				walls[i] = (int*) calloc(maxc, sizeof(int));
			}
		
		sl = 3;
		csl = 3;
		p = 1;
		fe = 0;
		s = 1;
		sf = 0;
		sc = 0;
		k = KEY_LEFT;
		lp = KEY_LEFT;
		t = 0;
		xt = 0;
		timeout(0);
		initSnake(snake, maxr, maxc, csnake, &e);
		placeWalls(walls, maxr, maxc);
		
		while(p){
			k = getCleanPress(&lp);
				
			if(k == 'p'){
				break;
			}
				
			spawnFood(food, maxr - 2, maxc - 2, walls, &e, &s, &sf, sc);
			moveSnake(snake, k, sl, maxr, maxc);
			moveCSnake(csnake, csl, maxr, maxc, food, walls, &lpc);
			checkDed(snake, csnake, walls, &sl, &csl, &fe, maxr, maxc, &p, t);
			checkEat(snake, food, &sl, &fe, k, csnake, &csl, &e, &s, &sc);
			checkStatus(&sf, &cdi, &sc, maxc, &di);
			drawWalls(walls, fe, maxr, maxc, p, cdi, &t, &xt);
			draw(snake, food, sl, csnake, csl, p, s, sf);
			
			//Works better than the simple sleep()
			struct timeval timeout;
			timeout.tv_sec = 0;
			timeout.tv_usec = 10000 * cdi;
			select(FD_SETSIZE, NULL, NULL, NULL, &timeout);
		}
		
		timeout(-1);
		if(getch() != 'y'){
			break;
		}
	}

	endwin();
	//SNAKE ME UP INSIDEE
	free(walls);
	free(snake);
	free(csnake);
	exit(0);
}